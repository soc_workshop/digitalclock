package com.example.lynn.digitalclock;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {
    public static Drawable[] drawables;
    public static Drawable colon;
    public static Drawable am;
    public static Drawable pm;
    public static ImageView[] views;
    public static MyThread myThread;
    public static Spinner timeZones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(new ClockView(this));
    }

    public void onDestroy() {
        super.onDestroy();

        myThread.stop();
    }
}
