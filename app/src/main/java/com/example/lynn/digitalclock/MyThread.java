package com.example.lynn.digitalclock;

/**
 * Created by lynn on 3/31/2017.
 */

import java.util.Calendar;
import java.util.TimeZone;

import static com.example.lynn.digitalclock.MainActivity.*;

public class MyThread implements Runnable {
    private boolean keepGoing;
    private Thread thread;

    public MyThread() {
        keepGoing = true;

        thread = new Thread(this);

        thread.start();
    }

    public void stop() {
        keepGoing = false;
    }

    private void pause(double seconds) {
        try {
            Thread.sleep((int)(seconds*1000));
        } catch (InterruptedException ie) {
            System.out.println(ie);
        }
    }

    private void getTime() {
        Calendar calendar = Calendar.getInstance();

        TimeZone timeZone = TimeZone.getTimeZone(timeZones.getSelectedItem().toString());

        calendar.setTimeZone(timeZone);

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);

        final boolean am = (hour < 12);

        hour -= (hour > 12) ? 12 : 0;

        final int hourFirstDigit = hour / 10;
        final int hourSecondDigit = hour % 10;

        final int minuteFirstDigit = minute / 10;
        final int minuteSecondDigit = minute % 10;

        final int secondFirstDigit = second / 10;
        final int secondSecondDigit = second % 10;

        views[0].post(new Runnable() {

            @Override
            public void run() {
                views[0].setImageDrawable(drawables[hourFirstDigit]);
                views[1].setImageDrawable(drawables[hourSecondDigit]);

                views[3].setImageDrawable(drawables[minuteFirstDigit]);
                views[4].setImageDrawable(drawables[minuteSecondDigit]);

                views[6].setImageDrawable(drawables[secondFirstDigit]);
                views[7].setImageDrawable(drawables[secondSecondDigit]);

                if (am)
                    views[8].setImageDrawable(MainActivity.am);
                else
                    views[8].setImageDrawable(pm);
            }
        });
    }

    @Override
    public void run() {
        while (keepGoing) {
            getTime();

            pause(0.5);
        }
    }

}

