package com.example.lynn.digitalclock;

import android.content.Context;
import android.graphics.drawable.Drawable;
import java.util.TimeZone;
import android.support.v4.content.ContextCompat;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.example.lynn.digitalclock.MainActivity.*;

/**
 * Created by lynn on 3/31/2017.
 */

public class ClockView extends LinearLayout {

    public ClockView(Context context) {
        super(context);

        drawables = new Drawable[]{ContextCompat.getDrawable(context,R.drawable.zero),
                                   ContextCompat.getDrawable(context,R.drawable.one),
                                   ContextCompat.getDrawable(context,R.drawable.two),
                                   ContextCompat.getDrawable(context,R.drawable.three),
                                   ContextCompat.getDrawable(context,R.drawable.four),
                                   ContextCompat.getDrawable(context,R.drawable.five),
                                   ContextCompat.getDrawable(context,R.drawable.six),
                                   ContextCompat.getDrawable(context,R.drawable.seven),
                                   ContextCompat.getDrawable(context,R.drawable.eight),
                                   ContextCompat.getDrawable(context,R.drawable.nine)};

        colon = ContextCompat.getDrawable(context,R.drawable.colon);

        am = ContextCompat.getDrawable(context,R.drawable.am);
        pm = ContextCompat.getDrawable(context,R.drawable.pm);

        views = new ImageView[9];

        for (int counter=0;counter<views.length;counter++)
            views[counter] = new ImageView(context);

        views[2].setImageDrawable(colon);
        views[5].setImageDrawable(colon);

        for (int counter=0;counter<views.length-1;counter++)
            if (counter != 2 && counter != 5)
                views[counter].setImageDrawable(drawables[0]);

        views[8].setImageDrawable(am);

        for (int counter=0;counter<views.length;counter++)
            addView(views[counter]);

        List<String> list = new ArrayList<>();

        list.addAll(Arrays.asList(TimeZone.getAvailableIDs()));

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item,list);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        timeZones = new Spinner(context);

        timeZones.setAdapter(adapter);

        addView(timeZones);


        myThread = new MyThread();
    }

}
